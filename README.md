# nodos

`nodos` helps you manage your GitLab Todos by marking as done those you are not
going to action. Currently `nodos` can close the following todos:
- Todos with group `mentions` or group `directly_addressed`
- Todos whose issue or MR has been closed or merged
- Todos for failed pipelines

## Usage
The `GITLAB_PAT` env var should contain a GitLab Personal Access Token(PAT)
with `api` scope.

```
$ bundle install
$ GITLAB_PAT=abcdefg ruby nodos.rb
Usage: nodos.rb <options>
    -g, --group                      Close group mentions or directly addressed
    -c, --closed                     Close if issue/MR has been closed or merged
    -b, --build                      Close if opened due to failed build
    -d, --dry-run                    Only print the todo ids that will be closed
    -h, --help                       Print this help
```

It is necessary to explicitly set the the flags for the closing criteria.
